<?php
// HTTP
define('HTTP_SERVER', 'http://lvivroyalcoffee.com/');

// HTTPS
define('HTTPS_SERVER', 'http://lvivroyalcoffee.com/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/domains/lvivroyalcoffee.com/catalog/');
define('DIR_SYSTEM', 'C:/OpenServer/domains/lvivroyalcoffee.com/system/');
define('DIR_IMAGE', 'C:/OpenServer/domains/lvivroyalcoffee.com/image/');
define('DIR_LANGUAGE', 'C:/OpenServer/domains/lvivroyalcoffee.com/catalog/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/domains/lvivroyalcoffee.com/catalog/view/theme/');
define('DIR_CONFIG', 'C:/OpenServer/domains/lvivroyalcoffee.com/system/config/');
define('DIR_CACHE', 'C:/OpenServer/domains/lvivroyalcoffee.com/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/domains/lvivroyalcoffee.com/system/storage/download/');
define('DIR_LOGS', 'C:/OpenServer/domains/lvivroyalcoffee.com/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OpenServer/domains/lvivroyalcoffee.com/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OpenServer/domains/lvivroyalcoffee.com/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'boiler_base');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
