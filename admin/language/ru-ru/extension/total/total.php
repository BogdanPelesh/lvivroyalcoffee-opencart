<?php
// Heading
$_['heading_title']    = 'Всього до сплати';

// Text
$_['text_extension']   = 'Загальна сума замовлення';
$_['text_success']     = 'Настройки модуля відновленні!';
$_['text_edit']        = 'Редактування модуля';

// Entry
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Порядок сортування:';

// Error
$_['error_permission'] = 'У вас нема прав для управлеіння цим модулем!';
