<?php
// Heading
$_['heading_title']    = 'Пропозиції тижня';

// Text
$_['text_extension']   = 'Модулі';
$_['text_success']     = 'Настройки модуля успішно обновлені!!';
$_['text_edit']        = 'Редактування модуля';

// Entry
$_['entry_name']       = 'Назва';
$_['entry_limit']      = 'Лиміт';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Висота';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас нема прав для управління модулем!';
$_['error_name']       = 'Назва модуля повинно бути від 3 до 64 символів!';
$_['error_width']      = 'Необхідно вказати ширину!';
$_['error_height']     = 'Необхідно вказати висоту!';
