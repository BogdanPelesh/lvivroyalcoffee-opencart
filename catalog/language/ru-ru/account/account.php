<?php
// Heading
$_['heading_title']      = 'Особисти кабінет';

// Text
$_['text_account']       = 'Особисти кабінет';
$_['text_my_account']    = 'Мій обліковий запис';
$_['text_my_orders']     = 'Мої замовлення';
$_['text_my_newsletter'] = 'Підписка';
$_['text_edit']          = 'Основні дані';
$_['text_password']      = 'Змінити свій пароль';
$_['text_address']       = 'Змінити мої адреса';
$_['text_credit_card']   = 'Менеджер карт оплати';
$_['text_wishlist']      = 'ИЗмінити закладки';
$_['text_order']         = 'Історія Замовлень';
$_['text_download']      = 'Файли для завантаження';
$_['text_reward']        = 'Бонусні бали';
$_['text_return']        = 'Заявки на повернення';
$_['text_transaction']   = 'Історія платежу';
$_['text_newsletter']    = 'Редактувати підписку';
$_['text_recurring']     = 'Регулярні платежі';
$_['text_transactions']  = 'Переводи';
