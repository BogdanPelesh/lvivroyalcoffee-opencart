<?php
// Heading
$_['heading_title']         = 'Історія замовлень';

// Button
$_['button_ocstore_payeer_onpay'] = 'Оплатити онлайн';
$_['button_ocstore_yk_onpay']     = 'Оплатити онлайн';

// Text
$_['text_account']          = 'Особистий кабінет';
$_['text_order']            = 'Замовлення';
$_['text_order_detail']     = 'Деталі замовлення';
$_['text_invoice_no']       = '№ Рахунку:';
$_['text_order_id']         = '№ Замовлення:';
$_['text_date_added']       = 'Добавлено:';
$_['text_shipping_address'] = 'Адрес доставки';
$_['text_shipping_method']  = 'Спосіб доставки:';
$_['text_payment_address']  = 'Платіжний адрес';
$_['text_payment_method']   = 'Спосіб оплати:';
$_['text_comment']          = 'Коментар до замовлення';
$_['text_history']          = 'Історія замовлень';
$_['text_success']          = 'Товар із замовлення <a href="%s">%s</a> добавлен в <a href="%s">Ваш кошик</a>!';
$_['text_empty']            = 'Ви ще не здійснили купівлі!';
$_['text_error']            = 'Запрошене замовлення не знайдено!';

// Column
$_['column_order_id']       = '№ Замовлення';
$_['column_customer']       = 'Покупець';
$_['column_product']        = 'Кількість товару';
$_['column_name']           = 'Найменування товару';
$_['column_model']          = 'Модель';
$_['column_quantity']       = 'Кількість';
$_['column_price']          = 'Ціна';
$_['column_total']          = 'Итого';
$_['column_action']         = 'Действие';
$_['column_date_added']     = 'Добавлено';
$_['column_status']         = 'Статус замовлення';
$_['column_comment']        = 'Коментарій';

// Error
$_['error_reorder']         = '%s в даний момент недоступений для замовлення.';
