<?php
// Locale
$_['code']                  = 'ru';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l d F Y';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'd.m.Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ' ';

// Text
$_['text_home']             = 'Головна';
$_['text_yes']              = 'Так';
$_['text_no']               = 'Ні';
$_['text_none']             = ' --- Не вибрано --- ';
$_['text_select']           = ' --- Виберіть --- ';
$_['text_all_zones']        = 'Всі регіони';
$_['text_pagination']       = 'Показано з %d по %d із %d (всього %d сорінок)';
$_['text_loading']          = 'Загрузка...';
$_['text_no_results']       = 'Нема результату!';

// Buttons
$_['button_address_add']    = 'Добавити адрес';
$_['button_back']           = 'Назад';
$_['button_continue']       = 'Продовжить';
$_['button_cart']           = 'В корзину';
$_['button_cancel']         = 'Відміна';
$_['button_compare']        = 'До порівняння';
$_['button_wishlist']       = 'До закладок';
$_['button_checkout']       = 'Оформлення замовлення';
$_['button_confirm']        = 'Підтвердження замовлення';
$_['button_coupon']         = 'Примінити купон';
$_['button_delete']         = 'Удалити';
$_['button_download']       = 'Завантажити';
$_['button_edit']           = 'Редагувати';
$_['button_filter']         = 'Уточнення пошуку';
$_['button_new_address']    = 'Новий адрес';
$_['button_change_address'] = 'Змінити адрес';
$_['button_reviews']        = 'Відгук';
$_['button_write']          = 'Написати відгук';
$_['button_login']          = 'Ввійти';
$_['button_update']         = 'Обновити';
$_['button_remove']         = 'Удалити';
$_['button_reorder']        = 'Додаткове замовлення';
$_['button_return']         = 'Повернення товару';
$_['button_shopping']       = 'Продовження купівлі';
$_['button_search']         = 'Пошук';
$_['button_shipping']       = 'Примінити доставку';
$_['button_submit']         = 'Відправити';
$_['button_guest']          = 'Оформленння замовлення без реєстрації';
$_['button_view']           = 'Подивитися';
$_['button_voucher']        = 'Прийняти сертифікат';
$_['button_upload']         = 'Загрузити файл';
$_['button_reward']         = 'Примінити бонусні бали';
$_['button_quote']          = 'Получити котирування';
$_['button_list']           = 'Список';
$_['button_grid']           = 'Сітка';
$_['button_map']            = 'Подивитися на Google карті';

// Error
$_['error_exception']       = 'Код ошибки (%s): %s в %s на строке %s';
$_['error_upload_1']        = 'Внимание: Размер загружаемого файла превышает значение директивы upload_max_filesize в php.ini!';
$_['error_upload_2']        = 'Внимание: Размер загружаемого файла превышает значение директивы MAX_FILE_SIZE, указанный в форме HTML!';
$_['error_upload_3']        = 'Предупреждение: Загруженный файл был загружен только частично!';
$_['error_upload_4']        = 'Внимание: Файл не был загружен!';
$_['error_upload_6']        = 'Предупреждение: Не найдена временная папка!';
$_['error_upload_7']        = 'Предупреждение: Не удалось записать файл на диск!';
$_['error_upload_8']        = 'Предупреждение: Загрузка файла была остановлена расширением!';
$_['error_upload_999']      = 'Предупреждение: Код ошибки не доступен!';
$_['error_curl']            = 'CURL: Код ошибки(%s): %s';
