<?php
// Heading
$_['heading_title']  = 'Контакти';

// Text
$_['text_location']  = 'Наша адреса';
$_['text_store']     = 'Наш магазин';
$_['text_contact']   = 'Форма';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Час роботи';
$_['text_comment']   = 'Коментарі';
$_['text_success']   = '<p>Ваше повідомлення відправленно!</p>';

// Entry
$_['entry_name']     = 'Імʼя';
$_['entry_email']    = 'Електронна адреса';
$_['entry_enquiry']  = 'Повідомлення';

// Email
$_['email_subject']  = 'Повідомлення %s';

// Errors
$_['error_name']     = 'Імʼя повинно бути від 3 до 32 символів!';
$_['error_email']    = 'Електронна адреса вказана не коректно!';
$_['error_enquiry']  = 'Повідомлення повинно бути від 10 до 3000 символів!';