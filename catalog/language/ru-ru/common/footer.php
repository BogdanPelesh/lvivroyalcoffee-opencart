<?php
// Text
$_['text_information']  = 'Інформація';
$_['text_service']      = 'Служба підтримки';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = 'Контакти';
$_['text_return']       = 'Повернення товару';
$_['text_sitemap']      = 'Карта сайтц';
$_['text_manufacturer'] = 'Виробники';
$_['text_voucher']      = 'Подарочні сертификати';
$_['text_affiliate']    = 'Партнери';
$_['text_special']      = 'Пропозиції тижня';
$_['text_account']      = 'Особистий кабінет';
$_['text_order']        = 'Історія замовлень';
$_['text_wishlist']     = 'Мої закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = 'Працює на <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';
