<?php
// Heading
$_['heading_title']            = 'Корзина';

// Text
$_['text_success']             = 'Товар <a href="%s">%s</a> добавлений у вашу <a href="%s">кошик</a>!';
$_['text_remove']              = 'Кошик успішно оновлений!';
$_['text_login']               = 'Вам необхідно <a href="%s">авторизуватися</a> чи <a href="%s">створити аккаунт</a> для просмотра цен!';
$_['text_items']               = '%s товар(ів) - %s';
$_['text_points']              = 'Призові бали: %s';
$_['text_next']                = 'Скоритчайтеся додатковити можливостями';
$_['text_next_choice']         = 'Якщо у вас є код купона на знижку чи бонусні бали, які ви хочете використовувати, виберіть відповідний пункт.';
$_['text_empty']               = 'В кошику пусто';
$_['text_day']                 = 'день';
$_['text_week']                = 'неділя';
$_['text_semi_month']          = 'півмісяця';
$_['text_month']               = 'місяць';
$_['text_year']                = 'рік';
$_['text_trial']               = '%s кожний %s %s для %s оплати ';
$_['text_recurring']           = '%s кожний %s %s';
$_['text_length']              = ' для %s оплат';
$_['text_until_cancelled']     = 'до відміни';
$_['text_recurring_item']      = 'Повторюючі';
$_['text_payment_recurring']   = 'Профіль платежу';
$_['text_trial_description']   = '%s кожний %d %s(s) для %d платежу(ів) тоді';
$_['text_payment_description'] = '%s кожний %d %s(s) для %d платежу(ів)';
$_['text_payment_cancel']      = '%s кожний %d %s(s) поки не будет відмінено';

// Column
$_['column_image']           = 'Картинка';
$_['column_name']            = 'Найменування товару';
$_['column_model']           = 'Марка';
$_['column_quantity']        = 'Кількість';
$_['column_price']           = 'Ціна за шт.';
$_['column_total']           = 'Всього';

// Error
$_['error_stock']              = 'Продукти відміченні *** відсутні в потрібній кількості чи їх нема в наявності!';
$_['error_minimum']            = 'Мінімальна кількість товару для замовлення %s рівна %s!';
$_['error_required']           = '%s необхідно!';
$_['error_product']            = 'У вашій козині нема товарів!';
$_['error_recurring_required'] = 'Виберіть платіжний профіль!';