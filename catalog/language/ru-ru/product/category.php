<?php
// Text
$_['text_refine']       = 'Уточнити пошук';
$_['text_product']      = 'Товари';
$_['text_error']        = 'Категорія не найдена!';
$_['text_empty']        = 'В цій категорії нема товарів.';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусні бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без налогу:';
$_['text_compare']      = 'Порівняння товарів (%s)';
$_['text_sort']         = 'Сортировать:';
$_['text_default']      = 'По замовчуванню';
$_['text_name_asc']     = 'По імені (A - Я)';
$_['text_name_desc']    = 'По імені (Я - A)';
$_['text_price_asc']    = 'По ціні (збільшенню)';
$_['text_price_desc']   = 'По ціні (зменшенню)';
$_['text_rating_asc']   = 'По рейтингу (збільшенню)';
$_['text_rating_desc']  = 'По рейтингу (зменшенню)';
$_['text_model_asc']    = 'По моделі (A - Я)';
$_['text_model_desc']   = 'По моделі (Я - A)';
$_['text_limit']        = 'Показувати:';
