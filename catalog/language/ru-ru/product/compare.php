<?php
// Heading
$_['heading_title']     = 'Порівняння товарів';

// Text
$_['text_product']      = 'інформація про товар';
$_['text_name']         = 'Найменування';
$_['text_image']        = 'Зображення';
$_['text_price']        = 'Ціна';
$_['text_model']        = 'Модель';
$_['text_manufacturer'] = 'Виробник';
$_['text_availability'] = 'Доступність';
$_['text_instock']      = 'В наявності';
$_['text_rating']       = 'Рейтинг';
$_['text_reviews']      = 'На підставі %s відгуку(ів).';
$_['text_summary']      = 'Summary';
$_['text_weight']       = 'Вага';
$_['text_dimension']    = '(Д x Ш x В)';
$_['text_compare']      = 'Порівняння товарів (%s)';
$_['text_success']      = 'Товар <a href="%s">%s</a> успішно добавлений в <a href="%s">список порівняння</a>!';
$_['text_remove']       = 'Список товаров успішно оновлений!';
$_['text_empty']        = 'Ви не вибрали ні одного товару для порівняння.';
