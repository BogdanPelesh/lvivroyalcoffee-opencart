<?php
// Heading
$_['heading_title']     = 'Пропозиції тижня';

// Text
$_['text_empty']        = 'В данний момент товар із знижкою не доступний';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model']        = 'Код товару:';
$_['text_points']       = 'Бонусні бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без НДС:';
$_['text_compare']      = 'Порівняння товарів (%s)';
$_['text_sort']         = 'Сортувати:';
$_['text_default']      = 'По замовчуванню';
$_['text_name_asc']     = 'По імені (А -Я)';
$_['text_name_desc']    = 'По по імені (Я - А)';
$_['text_price_asc']    = 'По ціні (збільшенню)';
$_['text_price_desc']   = 'По ціні (зменшенню)';
$_['text_rating_asc']   = 'По рейтингу (зменшенню)';
$_['text_rating_desc']  = 'По рейтингу (збільшенню)';
$_['text_model_asc']    = 'По моделі (А - Я)';
$_['text_model_desc']   = 'По моделі (Я - А)';
$_['text_limit']        = 'Показати:';
