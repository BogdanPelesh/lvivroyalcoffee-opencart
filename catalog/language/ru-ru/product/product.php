<?php
// Text
$_['text_search']                             = 'Пошук';
$_['text_brand']                              = 'Виробник';
$_['text_manufacturer']                       = 'Виробник:';
$_['text_model']                              = 'Код товару:';
$_['text_reward']                             = 'Бонусні бали:';
$_['text_points']                             = 'Ціна в бонусних балах:';
$_['text_stock']                              = 'Доступність:';
$_['text_instock']                            = 'На складі';
$_['text_tax']                                = 'Без НДС:';
$_['text_discount']                           = ' чи більше ';
$_['text_option']                             = 'Доступні опції';
$_['text_minimum']                            = 'Мінімальна кількість для замовлення цього товару: %s. ';
$_['text_reviews']                            = '%s відгуків';
$_['text_write']                              = 'Написати відгук';
$_['text_login']                              = 'Будь ласка, <a href="%s"> авторизуйтесь</a> чи <a href="%s"> зареєструйтесь</a> для перегляду';
$_['text_no_reviews']                         = 'Не має відгуків про даний товар.';
$_['text_note']                               = '<span class="text-danger"> HTML не підтримується! Використовуйте звичайний текст!';
$_['text_success']                            = 'Дякуємо за ваш відгук. Він був відправлений на модернізацію.';
$_['text_related']                            = 'Рекомедовані товари';
$_['text_tags']                               = 'Теги:';
$_['text_error']                              = 'Товар не найдений!';
$_['text_payment_recurring']                  = 'Платіжний профілі';
$_['text_trial_description']                  = '%s кожний %d %sй для %d оплат(и),';
$_['text_payment_description']                = '%s кожний %d %s(-и) із %d платежів(-а)';
$_['text_payment_cancel']                     = '%s every %d %s(s) until canceled';
$_['text_day']                                = 'день';
$_['text_week']                               = 'неділя';
$_['text_semi_month']                         = 'півмісяця';
$_['text_month']                              = 'місяць';
$_['text_year']                               = 'рік';

// Entry
$_['entry_qty']                               = 'кількість';
$_['entry_name']                              = 'Ваше імʼя';
$_['entry_review']                            = 'Коментар';
$_['entry_rating']                            = 'Оцінка';
$_['entry_good']                              = 'Чудовий';
$_['entry_bad']                               = 'Поганий';

// Tabs
$_['tab_description']                         = 'Опис';
$_['tab_attribute']                           = 'Характеристики';
$_['tab_review']                              = 'Відгуки (%s)';

// Error
$_['error_name']                              = 'Імʼя повинно бути від 3 до 25 символів!';
$_['error_text']                              = 'Текст відгуку повинен бути від 25 до 1000 символів!';
$_['error_rating']                            = 'Будь ласка, поставте оцінку!';
