<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div class="brand">
        <div class="container">
        <div class="bg-wrap">
            <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="items-wrap">
                            <div class="item">
                                <div class="logo"><img src="../catalog/view/theme/default/image/alpinella.png" alt="alpinella" /></div>
                                <p>Польська компанія "ALPINELLA" випускає чудову продукцію, яка спокійно може створити конкуренцію будь-якому, навіть самому знаменитому шоколадному бренду.Данная торгова лінія є досить різноманітною. Вона включає в себе як звичайнісінький шоколад, так і фігурну продукцію, шоколад з різними додатками і навіть діабетичний шоколад.</p>
                            </div>
                            <div class="item">
                                <div class="logo"><img src="../catalog/view/theme/default/image/torras.png" alt="torras" /></div>
                                <p>Іспанський шокодла "Torras" без цукру з похідником какао.
                                Технологічна угода про співпрацю з бельгійською компанією дозволило "Torras" розробити разом деякі формули і процеси виготовлення, що навіть сьогодні, тримають на дивуючи споживачів через їх вишуканий смак...
    </p>
                            </div>
                            <div class="item">
                                <div class="logo"><img src="../catalog/view/theme/default/image/jacobs.png" alt="jacobs" /></div>
                                <p>Марка кави "Jacobs" заснована 1895 року в Німеччині підприємцем Йоганном Якобсом. 
    На сьогоднішній день належить компанії Douwe Egberts. Це ароматна кава з різноматними смаками, яка завоювала великий ринок у різних країнах світу за свої смакові властивості.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <img class="img-brand" src="../catalog/view/theme/default/image/history-img.jpg" alt="brand">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo $footer; ?>