<a href="<?php echo $cart; ?>">
  <span class="icon-cart"></span>
  <span><?php echo $text_items; ?></span>
</a>
<ul class="menu-cart">
  <?php if ($products || $vouchers) { ?>
  <li>
    <div class="mini-cart-info">
      <table>
          <?php foreach ($products as $product) { ?>
        <tbody>
          <tr>
            <td class="image border">
              <?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
            <?php } ?>
            </td>
            <td class="name border">
              <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if ($product['option']) { ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
            <?php } ?>
            <?php } ?>
            <?php if ($product['recurring']) { ?>
            <br />
            - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
            <?php } ?>
              <br />
              <b><?php echo $product['quantity']; ?> x <?php echo $product['total']; ?></b>
            </td>
            <td class="remove border">
            <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>"><span class="icon-remove"></span></button>
            </td>
          </tr>
          <?php } ?>
          <?php foreach ($vouchers as $voucher) { ?>
          <tr>
            <td class="image border"></td>
            <td class="name border">
              <?php echo $voucher['description']; ?>
              <br />
              <b>1 x <?php echo $voucher['amount']; ?></b>
            </td>
            <td class="remove border">
            <button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><span class="icon-remove"></span></button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="min-cart-total">
      <table>
        <tbody>
        <?php foreach ($totals as $total) { ?>
          <tr>
            <td><strong><?php echo $total['title']; ?></strong></td>
            <td><?php echo $total['text']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="checkoutbuttons">
      <a href="<?php echo $cart; ?>" class="button"><?php echo $text_cart; ?></a>
      <a href="<?php echo $checkout; ?>" class="button"><?php echo $text_checkout; ?></a>
    </div>
  </li>
  <?php } else { ?>
  <li>
    <p class="text-center"><?php echo $text_empty; ?></p>
  </li>
  <?php } ?>
</ul>