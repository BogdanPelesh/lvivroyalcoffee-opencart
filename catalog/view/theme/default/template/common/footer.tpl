<footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <div class="footer-logo">
            <a href="">
              <img src="/catalog/view/theme/default/image/logo-footer.png" alt="logo footer">
            </a>
          </div>
        </div>
        <?php if ($informations) { ?>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <div class="footer-info">
            <h1><?php echo $text_information; ?></h1>
            <ul class="footer-menu">
            <?php foreach ($informations as $information) { ?>
              <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
              <?php } ?> 
              <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
            </ul>
          </div>
        </div>
        <?php } ?>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <div class="footer-product">
            <h1>Категорії</h1>
            <ul class="footer-product-items">
              <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=20">Кава</a></li>
              <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=18">Чай</a></li>
              <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=25">Гарячі напої</a></li>
              <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=62">Солодощі</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="footer-contact">
            <h1>Контакти</h1>
            <ul>
              <li><span class="icon-call"></span><a href="">+3 8(063) 635-18-95 </a></li>
              <li class="call"><a href="">+3 8(096) 635-18-95</a></li>
              <li><span class="icon-mail"></span><a href="mailto:support@lvivroyalcoffee.com">support@lvivroyalcoffee.com</a></li>
              <li class="card"><img src="catalog/view/theme/default/image/card.png" alt="card"></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <ul class="author">
          	<li>Інтернет-магазин розроблений і підтримується у компанії <a href="http://labbeat.pro" target="_blank">LaBBeat</a><sup>&#174;</sup></li>
          	<li class="year">2016-2017</li>
          </ul>
          <ul class="social">
            <li>Ми в соцмережах:</li>
            <li>
              <ul>
                <li><a href=""><span class="icon-vk"></span></a></li>
                <li><a href=""><span class="icon-facebook"></span></a></li>
                <li><a href=""><span class="icon-twitter"></span></a></li>
                <li><a href=""><span class="icon-skype"></span></a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <div id="hidden" class="mfp-content">
  <form id="small-dialog" class="zoom-anim-dialog mfp-hid" method="post" enctype="multipart/form-data">
    <div class="input-field-half">
            <h4>
              Ім'я
              <span class="required">*</span>
            </h4>
            <input type="hidden" name="project_name" value="Site Name">
            <input id="input-name" class="form-control" type="text" name="Name" placeholder="" required>
          </div>
          <div class="input-field-half">
            <h4>
            Телефон
              <span class="required">*</span>
            </h4>
            <input type="text" name="email" id="input-email" class="form-control">
          </div>
          <div class="button">
            <div class="pull-right">
              <input class="btn btn-primary" type="submit" value="Замовити">
            </div>
          </div>
  </form>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $(".popup-with-zoom-anim").magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in'
  });
});
</script>
</body></html>