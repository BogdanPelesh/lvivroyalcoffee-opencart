<?php echo $header; ?>
<div id="category">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="col-lg-9">
          <div class="caption">
            <h1><?php echo $heading_title; ?></h1>
          </div>
          <div class="product-filter">
            <div class="row">
              <div class="col-lg-6 col-sm-12">
                <div class="form-group input-group input-group-sm">
                    <label class="input-group-addon" for="input-sort"><?php echo $text_sort; ?></label>
                    <select id="input-sort" class="form-control" onchange="location = this.value;">
                      <?php foreach ($sorts as $sorts) { ?>
                      <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                      <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>    
                </div>
              </div>
              <div class="col-lg-6 col-sm-12">
                <div class="form-group input-group input-group-sm right">
                    <label class="input-group-addon" for="input-limit"><?php echo $text_limit; ?></label>
                    <select id="input-limit" class="form-control" onchange="location = this.value;">
                      <?php foreach ($limits as $limits) { ?>
                      <?php if ($limits['value'] == $limit) { ?>
                      <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>  
                </div>
              </div>
            </div>
          </div>
          <div class="product">
            <div class="row">
            <?php foreach ($products as $product) { ?>
              <div class="product col-lg-4 col-md-6 col-sm-12">
                <div class="item">
                  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <div class="info"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                  <div class="product-line"></div>
                  <div class="price-container">
                    <?php if ($product['price']) { ?>
                    <div class="price">
                      <?php if (!$product['special']) { ?>
                      <?php echo $product['price']; ?>
                      <?php } else { ?>
                      <span class="price-old"><?php echo $product['price']; ?></span><b><span class="price-new"><?php echo $product['special']; ?></span></b> 
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <?php if ($product['rating']) { ?>
                    <div class="rating">
                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                      <?php if ($product['rating'] < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } ?>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <button class="btn-price" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                  </div>
                </div>  
              </div>
              <?php } ?>
            </div>
          </div>
          <div id="pagination-wrap">
            <div class="row">
              <div class="col-lg-6">
                <div class="results">
                  <?php echo $results; ?>
                </div>
              </div>
              <div class="col-lg-6">
                  <?php echo $pagination; ?>
              </div>
            </div>
          </div>
          <?php if (!$categories && !$products) { ?>
          <p><?php echo $text_empty; ?></p>
          <div class="buttons">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
          </div>
          <?php } ?>
          <?php echo $content_bottom; ?>
          <?php echo $column_right; ?>
        </div>  
      </div>
    </div>
  </div>
<?php echo $footer; ?>
