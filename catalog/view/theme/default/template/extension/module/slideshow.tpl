
<div class="slider-none">
  <div class="slider owl-carousel owl-theme">
  <?php foreach ($banners as $banner) { ?>
    <div class="item">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
  </div>
</div>

<script type="text/javascript">
  $(".slider").owlCarousel({
    loop: true,
  items : 1,
  autoplay: true,
  smartSpeed:1000,
  autoplayTimeout: 3000,
  autoplayHoverPause:true,
  nav: true,
  navText: ["<span class=\"icon-angle-left\"></span>","<span class=\"icon-angle-right\"></span>"]
 });
</script>