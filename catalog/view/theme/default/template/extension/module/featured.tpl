<div class="products">
  <div class="product-heading">
    <h1><?php echo $heading_title; ?></h1>
    <div class="product-nav">
      <div class="prev prev-recommend">
        <span class="icon-angle-left"></span>
        </div>
      <div class="next next-recommend">
        <span class="icon-angle-right"></span>
      </div>
    </div>
  </div>
  <div class="product-wrap slider-featured owl-carousel owl-theme">
  <?php foreach ($products as $product) { ?>
    <div class="product-item">
      <div class="image">
        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
      </div>
      <div class="info"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <div class="product-line"></div>
      <div class="price-container">
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span><?php echo $product['special']; ?></span> <span><?php echo $product['price']; ?></span>
          <?php } ?>
        </div>
          <?php } ?>
        <button class="btn-price" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
      </div>
    </div>
    <?php } ?>
  </div> 
</div>
<script type="text/javascript">
   var recommend=$(".slider-featured");
 recommend.owlCarousel({
  loop: true,
  items : 4,
  responsiveClass:true,
  responsive:{
      0:{
          items:1,
          dots: false,
          nav: false,
      },
      768:{
          items:2,
          dots: false,
          nav: false,
      },
      992:{
          items:3,
          dots: false,
          nav: false,
      },
      1199:{
          items:4,
          dots: false,
          nav: false,
      }
  },
  dots: false,
  nav: false,
  navText: ["<span class=\"icon-angle-left\"></span>","<span class=\"icon-angle-right\"></span>"]
 });
  $(".next-recommend").click(function(){
    recommend.trigger('next.owl.carousel');
  })
  $(".prev-recommend").click(function(){
    recommend.trigger('prev.owl.carousel');
  })
</script>