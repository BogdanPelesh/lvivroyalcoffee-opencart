<?php echo $header; ?>
  <div class="container">
    <div class="row">
    <div class="col-lg-12">
      <ul class="breadcrumb">
        <li><a href="">Головна</a></li>
        <li><a href="">Контакти</a></li>
      </ul>
    </div>
    </div>
  </div>
  <div id="contact">
    <div class="container">
      <div class="row">
      <div class="col-lg-3">
        <aside id="column-left">
          <div class="caption">
            <h1>Категорії</h1>
          </div>
          <ul class="list-group">
            <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=20">Кава</a></li>
            <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=18">Чай</a></li>
            <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=25">Гарячі напої</a></li>
            <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=34">Солодощі</a></li>
            <li><a href="http://lvivroyalcoffee.com/index.php?route=product/category&path=57">Смаколики</a></li>
          </ul>
        </aside>
      </div>
      <div class="col-lg-3">
        <div class="caption">
          <h1>Контакти</h1>
        </div>
        <ul class="address">
          <li><span class="icon-call"></span><a href=""><?php echo $telephone; ?></a></li>
          <li class="call"><a href=""><?php echo $fax; ?></a></li>
          <li><span class="icon-mail"></span><a href="mailto:support@lvivroyalcoffee.com">support@lvivroyalcoffee.com</a></li>
        </ul>
        <ul class="social">
          <li>Ми в соцмережах:</li>
          <li>
            <ul class="social-icon">
              <li><a href=""><span class="icon-vk"></span></a></li>
              <li><a href=""><span class="icon-facebook"></span></a></li>
              <li><a href=""><span class="icon-twitter"></span></a></li>
              <li><a href=""><span class="icon-skype"></span></a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="col-lg-6">
        <div class="caption">
          <h1>Контактна форма</h1>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="input-field-half">
            <h4>
              <?php echo $entry_name; ?>
              <span class="required">*</span>
            </h4>
            <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control">
            <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
            <?php } ?>
          </div>
          <div class="input-field-half right">
            <h4>
            <?php echo $entry_email; ?>
              <span class="required">*</span>
            </h4>
            <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control">
            <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
            <?php } ?>
          </div>
          <div class="input-field-full">
            <h4>
            <?php echo $entry_enquiry; ?>
              <span class="required">*</span>
            </h4>
            <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
            <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
            <?php } ?>
          </div>
          <?php echo $captcha; ?>
          <div class="button">
            <div class="pull-right">
              <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>">
            </div>
          </div>
        </form>
        <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
      </div>
      </div>
    </div>
  </div>
<?php echo $footer; ?>
